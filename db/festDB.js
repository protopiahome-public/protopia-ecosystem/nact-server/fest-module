const fs = require('fs');

export default festID => {
    const mongoat = require('../../../db/mongoat')
    let db
    if (fs.existsSync(`${__dirname}/../../../config/db_config.json`)) 
    {
        const config = require('../../../config/db_config')
        let userString = ''
        if (config.user && config.password) {
            userString = `${config.user}:${config.password}@`
        }
        const connectionString = `mongodb://${userString}${config.host}:${config.port}/pe_fest_${festID}`
        db = mongoat(connectionString)
    }
    return db
}