import { ApolloError } from "apollo-server";

export class FestivalExistsError extends ApolloError {
    constructor(message ) {
        super(message, 'FESTIVAL_EXISTS_ERROR');
        Object.defineProperty(this, 'name', { value: 'FestivalExistsError' });
    }
} 