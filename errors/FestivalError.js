import { ApolloError } from "apollo-server";

export class FestivalError extends ApolloError {
    constructor(message ) {
        super(message, 'FESTIVAL_ERROR');
        Object.defineProperty(this, 'name', { value: 'FestivalError: ' });
    }
} 