
import { query } from "nact";
import { FestivalError } from "../errors/FestivalError";
import { FestivalExistsError } from "../errors/FestivalExistsError";
import InsertFestivalResponse from "../response/InsertFestivalResponse";

export default async (obj, args, ctx, info) => {
    const input = args.input
    const collectionItemActor = ctx.children.get('item');
    if(!input.festivalId)
    {
        throw new FestivalError("Non-empty field expected: festivalId")
    }

    // проверяем: есть ли уже Фестиваль с тем же festivalID?
    let candidate = ( 
        await query(
          collectionItemActor, 
          { 
            type: 'festival', 
            search: { festivalId: args.input.festivalId } 
          }, 
          process.env.ACTOR_TIMEOUT
        )
    ) 
    if(candidate)
    {
        throw new FestivalExistsError("This festival is already registered.Field festivalId must be uniq.")
    } 

    const result = await query(collectionItemActor, { type: 'festival', input }, process.env.ACTOR_TIMEOUT);
    //
    return InsertFestivalResponse({
        ...result,
        id: result._id
    })
}