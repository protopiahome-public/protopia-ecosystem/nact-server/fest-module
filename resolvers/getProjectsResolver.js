import { query } from "nact"
import { FestivalError } from "../errors/FestivalError"
import parseTypeByName from "../../graphql-core-module/core/parseTypeByName"

export default async (obj, args, ctx, info) => {
    const collectionActor = ctx.children.get('collection');
    let projectsData =  await query(collectionActor, { type: 'project' }, process.env.ACTOR_TIMEOUT);  
    if( !Array.isArray(projectsData) )
    {
        throw new FestivalError("Projects not exists.")
    }
    projectsData = projectsData.map(f => ({ ...f, id: f._id }))
    const ret = projectsData.map( fData => parseTypeByName( "Project", ctx, fData, info ) ) 
    return ret
}