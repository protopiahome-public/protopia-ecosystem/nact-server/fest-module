import { query } from "nact";
import { FestivalError } from "../errors/FestivalError";

export default async (obj, args, ctx, info) => {
    const input = args.input
    const collectionItemActor = ctx.children.get('item');

    await query(collectionItemActor, { type: 'festival', input }, process.env.ACTOR_TIMEOUT);
    
    const result = await query(
        collectionItemActor, 
        { 
            type: 'festival', 
            search: { _id: args.id }, 
            input: args.input 
        }, 
        process.env.ACTOR_TIMEOUT
    );
    //
    return {
        ...result,
        id: result._id
    }
}