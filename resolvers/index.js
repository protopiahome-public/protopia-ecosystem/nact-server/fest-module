const { GraphQLScalarType } = require("graphql") 
const { default: insertFestivalResolver } = require("./insertFestivalResolver")
const { default: changeFestivalResolver } = require("./changeFestivalResolver")
const { default: getFestivalsResolver } = require("./getFestivalsResolver")
const { default: getFestFamiliesResolver } = require("./getFestFamiliesResolver")
const { default: getDomainTypesResolver } = require("./getDomainTypesResolver")
const { default: getExtendFieldsResolver } = require("./getExtendFieldsResolver")
const { default: getPostsResolver } = require("./getPostsResolver")
const { default: getFestivalCountResolver } = require("./getFestivalCountResolver")
const { default: getProjectsResolver } = require("./getProjectsResolver")  
const { default: getTracksResolver } = require("./getTracksResolver") 
const { default: getHoneycombssResolver } = require("./getHoneycombssResolver") 
const { default: getGanresResolver } = require("./getGanresResolver")
const { default: getOneByType } = require("../../graphql-core-module/core/getOneByType")
const { default: getCollectionByType } = require("../../graphql-core-module/core/getCollectionByType")
const { default: getCollectionCountByType } = require("../../graphql-core-module/core/getCollectionCountByType")
const { default: insertByType } = require("../../graphql-core-module/core/insertByType")
const { default: changeOneByType } = require("../../graphql-core-module/core/changeOneByType")

/*
*   This is example of resolvers.
*   Change this file structure for your tasks
*/
module.exports = {
    Mutation: { 
      insertFestival: (obj, args, ctx, info) => insertFestivalResolver(obj, args, ctx, info), 
      insertFestFamily: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'festFamily'),
      insertDomainType: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'domainType'),
      insertTrack: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'track'),
      insertHoneycombs: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'honeycombs'),
      insertGanre: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'ganre'),
      insertPost: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'post'),
      insertProject: (obj, args, ctx, info) => insertByType(obj, args, ctx, info, 'project'),

      changeFestival: (obj, args, ctx, info) => changeOneByType(obj, args, ctx, info, "festival", "Festival"),
    },
    Query: { 
      getFestival: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, "festival", "Festival"),
      getFestivals: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, "festival", "Festival"),
      getFestivalCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, "festival"),

      getFestFamily: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, 'festFamily', "FestFamily"),
      getFestFamilies: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, 'festFamily', "FestFamily"),
      getFestFamilyCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, 'festFamily'),
      
      getDomainType: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, 'domainType', "DomainType"),
      getDomainTypes: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, 'domainType', "DomainType"),
      getDomainTypeCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, 'domainType'),

      getTrack: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, 'track', "Track"),
      getTracks: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, 'track', "Track"),
      getTrackCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, 'track'),

      getHoneycombs: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, 'honeycombs', 'Honeycombs'),
      getHoneycombss: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, 'honeycombs', 'Honeycombs' ),
      getHoneycombsCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, 'honeycombs'),

      getGanre: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, 'ganre', 'Ganre'),
      getGanres: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, 'ganre', 'Ganre' ),
      getGanreCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, 'ganre'), 

      getExtendFields: (obj, args, ctx, info) => getExtendFieldsResolver(obj, args, ctx, info),
      
      getPosts: (obj, args, ctx, info) => getPostsResolver(obj, args, ctx, info),

      getProject: (obj, args, ctx, info) => getOneByType(obj, args, ctx, info, "project", 'Project'),
      getProjects: (obj, args, ctx, info) => getCollectionByType(obj, args, ctx, info, 'project', 'Project'),
      getProjectCount: (obj, args, ctx, info) => getCollectionCountByType(obj, args, ctx, info, "project"), 

    },
    ProjectResultType: new GraphQLScalarType({
      name: 'ProjectResultType',
      serialize: value => value.toString(),
      parseValue: value => value.toString(),
      parseLiteral: ast => ast.value.toString()
    }),
  };