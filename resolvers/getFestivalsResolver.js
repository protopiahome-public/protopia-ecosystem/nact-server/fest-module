import { query } from "nact"; 
import { FestivalError } from "../errors/FestivalError";  
import parseTypeByName from "../../graphql-core-module/core/parseTypeByName"

// import festDB from "../db/festDB"

export default async (bj, args, ctx, info) => {
    const collectionActor = ctx.children.get('collection');
    let festivalsData =  await query(collectionActor, { type: 'festival' }, process.env.ACTOR_TIMEOUT);  
    if( !Array.isArray(festivalsData) )
    {
        throw new FestivalError("Festivals not exists.")
    }
    const festivalData = festivalsData.map(f => ({ ...f, id: f._id }))
    
    // info.fieldNodes[0].selectionSet.selections.forEach(ss => {
    //     console.log( ss.name.value )
    // })
    /*
    info.fieldNodes[0].selectionSet.selections.forEach(ss => {
        console.log( ss.name.value )
        if( ss.selectionSet )
        {
            //console.log("   ", ss.selectionSet.selections )
            ss.selectionSet.selections.forEach(sss => {
                console.log("   ", sss.name.value )
                if(sss.selectionSet)
                {
                    sss.selectionSet.selections.forEach(ssss => {
                        console.log("       ", ssss.name.value)
                        if( ssss.selectionSet )
                        {
                            ssss.selectionSet.selections.forEach(sssss => {
                                console.log("           ", sssss.name.value )
                                if(sssss.selectionSet)
                                {
                                    sssss.selectionSet.selections.forEach(ssssss => {
                                        console.log("               ", ssssss.name.value)
                                    })
                                } 
                            })
                        }
                    })
                } 
            })
            //console.log("   ",  ss.selectionSet.selections[0].name.value)
            //console.log( ss.selectionSet.selections )
           
        }
        console.log(" ")
    })  
    */
    
    // console.log( info )
    // console.log( info.fieldNodes[0].arguments[0].value.fields )
    const ret = await Promise.all(festivalData.map( fData => parseTypeByName( "Festival", ctx, fData, info ) ) )
    // console.log( ret )
    return ret 
}