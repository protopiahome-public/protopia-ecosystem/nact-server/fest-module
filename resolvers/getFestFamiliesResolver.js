import { query } from "nact"
import { FestivalError } from "../errors/FestivalError"
import parseTypeByName from "../../graphql-core-module/core/parseTypeByName"

export default async (obj, args, ctx, info) => {
    const collectionActor = ctx.children.get('collection');
    let elementsData =  await query(collectionActor, { type: 'festFamily' }, process.env.ACTOR_TIMEOUT);  
    if( !Array.isArray(elementsData))
    {
        throw new FestivalError("Festival Family not exists.")
    }
    elementsData = elementsData.map( f => ({ ...f, id: f._id }) )
    const ret = elementsData.map( fData => parseTypeByName( "FestFamily", ctx, fData, info ) ) 
    return ret
}